import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BaseRedirectComponent} from './base-redirect.component';

describe('BaseRedirectComponent', () => {
  let component: BaseRedirectComponent;
  let fixture: ComponentFixture<BaseRedirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BaseRedirectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
