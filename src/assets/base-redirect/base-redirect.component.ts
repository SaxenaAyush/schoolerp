import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { SchoolErpHttpService } from '../../app/services/school-erp-http.service';
import { SchoolErpStorageService } from '../../app/services/school-erp-storage.service';

@Component({
  selector: 'app-base-redirect',
  templateUrl: './base-redirect.component.html',
  styleUrls: ['./base-redirect.component.css']
})
export class BaseRedirectComponent implements OnInit {

  constructor(private storageService: SchoolErpStorageService, private router: Router) {
    if (this.storageService.get('currentUser')) {
      if (this.storageService.get('currentUser').guestUser) {
        this.router.navigate(['/student/dashboard']);
      } else if (!this.storageService.get('currentUser').guestUser) {
        this.router.navigate(['/authority/dashboard']);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
  }

}
