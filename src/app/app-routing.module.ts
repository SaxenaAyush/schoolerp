import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginLayoutComponent } from './Components/layout/login-layout/login-layout.component';
import { HomeLayoutComponent } from './Components/layout/home-layout/home-layout.component';
import { LoginComponent } from './Components/login/login.component';
import { BaseRedirectComponent } from '../assets/base-redirect/base-redirect.component';
import { StudentDashboardComponent } from './Components/Students/student-dashboard/student-dashboard.component';
import { DashboardComponent } from './Components/Authority/dashboard/dashboard.component';
import { BusDetailsComponent } from './Components/Students/bus-details/bus-details.component';
import { HomeComponent } from './Components/home/home.component';


const routes: Routes = [
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
    
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
    ]
  },
  {
    path: 'authority',
    component: HomeLayoutComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: 'Authority-dashboard',
        component: DashboardComponent
      },
    ]
  },
  {
    path: 'student',
    component: HomeLayoutComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: 'Student-dashboard',
        component: StudentDashboardComponent
      },
      {
        path: 'bus-details',
        component: BusDetailsComponent
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
