import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeLayoutComponent } from './Components/layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from './Components/layout/login-layout/login-layout.component';
import { LoginComponent } from './Components/login/login.component';
import { BaseRedirectComponent } from '../assets/base-redirect/base-redirect.component';
import { SidebarComponent } from './Components/sidebar/sidebar.component';
import { StudentDashboardComponent } from './Components/Students/student-dashboard/student-dashboard.component';
import { HeaderComponent } from './Components/header/header.component';
import { DashboardComponent } from './Components/Authority/dashboard/dashboard.component';
import { BusDetailsComponent } from './Components/Students/bus-details/bus-details.component';
import { HomeComponent } from './Components/home/home.component';
import {CarouselModule} from 'ngx-owl-carousel-o';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    LoginComponent,
    BaseRedirectComponent,
    SidebarComponent,
    StudentDashboardComponent,
    HeaderComponent,
    DashboardComponent,
    BusDetailsComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule,
    BrowserAnimationsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
