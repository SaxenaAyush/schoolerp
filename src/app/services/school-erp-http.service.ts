import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { ConstantsHelper } from '../constants/constant';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SchoolErpHttpService {
  datatransfer: any;

  constructor(private http: HttpClient, private router: Router) {
  }

  public makeRequestApi(type, urlName, payload?, params?) {
    // console.log(type, urlName, payload, params);
    const apiUrl = ConstantsHelper.getAPIUrl[urlName](params);
    // console.log(apiUrl);
    switch (type) {
      case 'get':
        const headerOpt = payload ? {} : {
          headers: {
            apikey: 'BaLa@123'
          }
        };
        return this.http.get(apiUrl, headerOpt).pipe(
          tap((result: any) => {

          }),
          catchError(this.handleError())
        );

      // break;
      case 'post':
        return this.http.post(apiUrl, JSON.stringify(payload))
        // .retry(0)
        // .timeout(8000)
          .pipe(
            tap((result: any) => {
              // this.extractData(result)
            }),
            catchError(this.handleError())
          );
    }

  }

  // private extractData(result) {
  //   return result.data
  // }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // this.router.navigate(['/login']);
      let errorMessage = null;
      if (error.error instanceof ErrorEvent) {
        errorMessage = 'An error occurred:' + ',' + error.error.message;
      } else {
        // console.log(error.status);

        if (error.status === 400) {
          // this.alertService.swalError('Your session has been expired. Please login again.');
          this.router.navigate(['/login']);
        }
        if (error.status === 401) {
          // this.alertService.swalError('Unauthorised user!');
          this.router.navigate(['/login']);
        }
        if (error.status === 500) {
          // this.alertService.swalError('Got some trouble, please try again!');
          // this.router.navigate(['/landing']);
        }
        errorMessage = `${error.error.message}`;
      }
      // this.log(`${operation} failed: ${error.message}`);
      // this.alertService.swalError();
      return throwError(errorMessage = errorMessage ? errorMessage : 'Something bad happened; please try again later.');
    };
  }

  private log(message: string) {
    // console.log(message);
    // this.messageService.add('HeroService: ' + message);
  }
}

export class ApiInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // req = req.clone({ headers: req.headers.set('Access-Control-Allow-Origin', '*').set('Content-Type', 'application/json').set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept').set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT').set('withCredentials', 'true') });
    // if (req.method != 'GET')
    req = req.clone({headers: req.headers.set('Content-Type', 'application/json')});
    // req = req.clone({
    //   headers: req.headers.set('Access-Control-Allow-Origin', '*').set('Content-Type', 'application/json').set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept').set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT')
    // })

    // req = req.clone({ headers: req.headers.set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept') });
    // req = req.clone({ headers: req.headers.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT') });
    // req = req.clone({ params: req.params.set('filter', 'completed') });

    return next.handle(req);
  }
}
