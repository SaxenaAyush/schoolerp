import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SchoolErpStorageService {

     constructor() {
      if (!this.get('currentUser')) {
        localStorage.setItem('SchoolErp', '');
      }
    }
  
    get(key?: string) {
      const val = JSON.parse(decodeURIComponent(escape(localStorage.getItem('SchoolErp') ?
        atob(localStorage.getItem('SchoolErp')) : '{}')));
      return val[key];
    }
  
    set(key: string, val: any) {
      const all = JSON.parse(decodeURIComponent(escape(localStorage.getItem('SchoolErp') ?
        atob(localStorage.getItem('SchoolErp')) : '{}')));
      all[key] = val;
      return localStorage.setItem('SchoolErp', btoa(unescape(encodeURIComponent(JSON.stringify(all)))));
    }
  
    remove(key) {
      const all = JSON.parse(decodeURIComponent(escape(localStorage.getItem('SchoolErp') ?
        atob(localStorage.getItem('SchoolErp')) : '{}')));
      delete all[key];
      localStorage.setItem('SchoolErp', btoa(unescape(encodeURIComponent(JSON.stringify(all)))));
      return true;
    }
  }
