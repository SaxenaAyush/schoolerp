import { TestBed } from '@angular/core/testing';

import { SchoolErpStorageService } from './school-erp-storage.service';

describe('SchoolErpStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SchoolErpStorageService = TestBed.get(SchoolErpStorageService);
    expect(service).toBeTruthy();
  });
});
