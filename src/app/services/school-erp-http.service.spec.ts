import { TestBed } from '@angular/core/testing';

import { SchoolErpHttpService } from './school-erp-http.service';

describe('SchoolErpHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SchoolErpHttpService = TestBed.get(SchoolErpHttpService);
    expect(service).toBeTruthy();
  });
});
