import { Component, OnInit } from '@angular/core';
import $ from "jquery";
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
    $('.navigation-main').find('li').has('ul').children('a').on('click', function (e) {
      e.preventDefault();

      // Collapsible
      $(this).parent('li').not('.disabled').not($('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).toggleClass('active').children('ul').slideToggle(250);

      // Accordion
      if ($('.navigation-main').hasClass('navigation-accordion')) {
          $(this).parent('li').not('.disabled').not($('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).siblings(':has(.has-ul)').removeClass('active').children('ul').slideUp(250);
      }
  });

      
  // Alternate navigation
  $('.navigation-alt').find('li').has('ul').children('a').on('click', function (e) {
      e.preventDefault();

      // Collapsible
      $(this).parent('li').not('.disabled').toggleClass('active').children('ul').slideToggle(200);

      // Accordion
      if ($('.navigation-alt').hasClass('navigation-accordion')) {
          $(this).parent('li').not('.disabled').siblings(':has(.has-ul)').removeClass('active').children('ul').slideUp(200);
      }
  }); 

// Main navigation
  // -------------------------

  // Add 'active' class to parent list item in all levels
  $('.navigation').find('li.active').parents('li').addClass('active');

  // Hide all nested lists
  $('.navigation').find('li').not('.active, .category-title').has('ul').children('ul').addClass('hidden-ul');

  // Highlight children links
  $('.navigation').find('li').has('ul').children('a').addClass('has-ul');

  // Add active state to all dropdown parent levels
  $('.dropdown-menu:not(.dropdown-content), .dropdown-menu:not(.dropdown-content) .dropdown-submenu').has('li.active').addClass('active').parents('.navbar-nav .dropdown:not(.language-switch), .navbar-nav .dropup:not(.language-switch)').addClass('active');

  }
BusDetails(){
  this.router.navigate(['/student/bus-details'])
}
}
