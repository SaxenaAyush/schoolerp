import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userList = [
    {
      id: 'Authority',
      password: 12345
    },
    {
      id: 'Student',
      password: 12345
    }
  ];
public userForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      usrName: ['', [Validators.required]],

      password: ['', [Validators.required]],

    });

  }
Login(){
  const usr = this.userList.filter(usr => usr.id == this.userForm.controls.usrName.value);
  const pass = this.userList.filter(pass => pass.password == this.userForm.controls.password.value);
  if (usr.length > 0) {
    if (usr[0].id == 'Authority' && pass[0].password == 12345) {
      this.router.navigate(['authority//Authority-dashboard'])
      console.log("if working");

    } else if (usr[0].id == 'Student' && pass[0].password == 12345) {
      this.router.navigate(['/student/Student-dashboard'])
      console.log("else working");
    }
  } else {
    alert('invalid user id')
  }
}
}
