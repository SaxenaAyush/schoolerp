import {NgModule, ModuleWithProviders} from '@angular/core';
import {SweetAlertService} from './../sweetalert2.service';
import { SharedServiceService } from '../shared-service.service';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [CommonModule, NgbModule.forRoot()],
  declarations: [],
  providers: [SweetAlertService],
  exports: [NgbModule]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [SweetAlertService, , SharedServiceService]
    };
  }
}
